{
    const modal = document.querySelector('.ca-modal')
    const key = 'ca-modal-hide'

    function hide() {
        modal.style.display = 'none'
    }

    document.addEventListener('click', (e) => {
        if (e.target === modal) {
            hide()
        }
    })

    document.querySelector('.ca-modal-exout').addEventListener('click', hide)
    document
        .querySelector('.ca-modal-subscribe')
        .addEventListener('click', () => {
            hide()
            location.href = '/'
        })

    document.getElementById('dev-button').addEventListener('click', () => {
        if (!localStorage.getItem(key)) {
            localStorage.setItem(key, 1)
            modal.style.display = 'block'
        }
    })

    document.getElementById('dev-button-2').addEventListener('click', () => {
        localStorage.removeItem(key)
    })
}
