# modal

This modal displays once before placing a key in `localStorage` that keeps it hidden; built desktop-first.
